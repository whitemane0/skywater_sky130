#!/usr/bin/tclsh

#########################################################################
# Generation of mmmc.tcl (MMMC config file for Genus MMMC flow)			#
# with all available corners   											#
# Common UI mode														#
# $path, $prefix, $name, $process, $voltage, $suffix and $temperature   #
# variables should be replaced to match liberty file naming convention 	#
# of particular logic library vendor									#
# Slight modification may be needed for other vendor					#
# (c) 2020 Vladimir W. Belyaev                                      	#
# mailto:me@whiteman.ru                                             	#
# https://whiteman.ru                                               	#
#########################################################################

set path "/auto/foundry/skywater/sky130_osu_sc_t18/18T_ls/lib/"
set prefix "wcl_"
set name "sky130_osu_sc_18T_ls_"
set process { "ss_" "tt_" "ff_"  }
set voltage { "1P20" "1P28" "1P35" "1P44" "1P50" "1P56" "1P60" "1P62" "1P68" "1P76" "1P80" "1P8" "1P89" "1P95" "2P10" }
set suffix "_"
set temperature { "-40C" "25C" "100C" "150C" }
set all_analysis_views ""
set p_symbol { "P" }
set v_symbol { "" }
set m_symbol { "" }
set c_symbol { "C" }
set group_delimeter ""

lappend p_symbol "\."
lappend v_symbol ""
lappend m_symbol ""
lappend c_symbol ""

puts "$temperature"
puts "$p_symbol $v_symbol $m_symbol $c_symbol"

#exit

exec echo #MMMC config file for $name library set > mmmc.tcl

exec echo create_rc_corner -name rc_corner >> mmmc.tcl
exec echo create_constraint_mode -name functional_a -sdc_files \{ ./SDC/1000.sdc \} >> mmmc.tcl

foreach p $process {
	foreach v $voltage {
		foreach t $temperature {
			if { [file exists "$path$name$p$v$suffix$group_delimeter$t\.ccs\.lib" ] == 1 } {
				exec echo create_library_set -name $prefix$p$v$group_delimeter$t -timing \{ $path$name$p$v$suffix$group_delimeter$t\.ccs\.lib \} >> mmmc.tcl
				puts "file $path$name$p$v$suffix$group_delimeter$t\.ccs\.lib written to mmmc.tcl"
				set v1 [string map $p_symbol $v]
				set v1 [string map $v_symbol $v1]
				set t1 [string map $m_symbol $t]
				set t1 [string map $c_symbol $t1]
				puts "$v1 $t1"
				puts "opcond for $p ${v1}V ${t1}C written to mmmc.tcl"
				exec echo create_opcond -name op_cond_$prefix$p$v$group_delimeter$t -process 1 -voltage $v1 -temperature $t1 >> mmmc.tcl
				exec echo create_timing_condition -name timing_cond_$prefix$p$v$group_delimeter$t -opcond op_cond_$prefix$p$v$group_delimeter$t   -library_sets \{ $prefix$p$v$group_delimeter$t \} >> mmmc.tcl
				exec echo create_delay_corner -name delay_corner_$prefix$p$v$group_delimeter$t -early_timing_condition timing_cond_$prefix$p$v$group_delimeter$t -late_timing_condition timing_cond_$prefix$p$v$group_delimeter$t -early_rc_corner rc_corner -late_rc_corner rc_corner >> mmmc.tcl
				exec echo create_analysis_view -name view_$prefix$p$v$group_delimeter$t -constraint_mode functional_a -delay_corner delay_corner_$prefix$p$v$group_delimeter$t >> mmmc.tcl
				lappend all_analysis_views  view_$prefix$p$v$group_delimeter$t
			} else { puts "file $path$name$p$v$suffix$group_delimeter$t\.ccs\.lib not found" }
			
		}
	}
}

exec echo set_analysis_view -setup \{ $all_analysis_views \} >> mmmc.tcl

